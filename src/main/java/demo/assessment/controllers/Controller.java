package  demo.assessment.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.assessment.models.ServiceResponse;
import demo.assessment.service.CitiesAutoCompetionService;

/**
 * Created by yogesh mungali   
 * We would like you to create a java based backend application using REST.
 *
 */
@RestController
public class Controller {
	
	 private static final Logger LOG = LoggerFactory.getLogger(Controller.class);
	
	 @Autowired
	 private CitiesAutoCompetionService citiesAutoCompetionService;
	 /*@Value("${demo.microservice.basepath}")
	    private String basePath;

	 @Value("${local.server.port}")
	 private int port;*/
	    

	
	/**
	 * get a list of Cities  
	 * http://localhost:8080/suggest_cities?start=che&atmost=5
	 * @return
	 */
	@RequestMapping(value = "/suggest_cities", method = RequestMethod.GET)
	public ServiceResponse getSuggestCities(@RequestParam("start") String start,
			@RequestParam("atmost") int atmost) {
		LOG.info(Controller.class.getSimpleName() + " received a suggest cities request."); 
		
		List<String> citiesList = citiesAutoCompetionService.getCitiesList(start, atmost);		
		ServiceResponse serviceResponse = new ServiceResponse();
		 
		 LOG.info(Controller.class.getSimpleName() + " is responding with: " + citiesList);
		 
		 if(citiesList.isEmpty())
		 {
			serviceResponse.setMessage("No matching state found for requested code");
			serviceResponse.setStatus( HttpStatus.NOT_FOUND.value());
			return serviceResponse;
		 }
		 else
		 {
			serviceResponse.setMessage("matching cities found for requested code!!!");
			serviceResponse.setResult(citiesList, atmost );
			return serviceResponse;
				
		 }
			
			

	}
	
 
	
}
