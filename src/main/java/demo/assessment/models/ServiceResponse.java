package demo.assessment.models;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
 


@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE)
public class ServiceResponse {
	
	 @JsonProperty
	 private String message = null;
	 
	  @JsonProperty
	  private Integer status = HttpStatus.OK.value();

	  @JsonProperty
	  private List<String> result = new ArrayList<String>();

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<String> getResult() {
		return result;
	}

	public void setResult(List<String> result, int atmost) {
		if(result.size() > atmost)
		{
			for(int i=0; i< atmost; i++)
			{
				this.result.add(result.get(i));
			}
		}
		else
		{
			this.result = result;
		}
		
		
	}
}
