package demo.assessment.utils;

import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonUtils {
    public static ObjectNode mergeObjectNodes(final ObjectNode node1, final ObjectNode node2) {
        node2.fieldNames().forEachRemaining((String fieldName) -> node1.replace(fieldName, node2.get(fieldName)));
        return node1;
    }
}
