package demo.assessment.service;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import demo.assessment.models.Cities;

/**
 * Created by yogesh mungali.
 */
@Service
public class CitiesAutoCompetionService {
	
	List<String> cities = Arrays.asList("Java", "Scala", "C++", "Haskell", "Lisp");

 
	 private static final Logger LOG = LoggerFactory.getLogger(CitiesAutoCompetionService.class);

	    public CitiesAutoCompetionService() {
	    }

	
	    public List<String> getCitiesList(String start, int atmost) {
	    	LOG.info(CitiesAutoCompetionService.class.getSimpleName() + " received a suggest cities request.");
	    	LOG.info(CitiesAutoCompetionService.class.getSimpleName() + " received param.start ="+start);
	    	LOG.info(CitiesAutoCompetionService.class.getSimpleName() + " received param.atmost ="+atmost);
	    	Predicate<String> startsWithJ = (n) -> n.startsWith(start);  
	    	   
	    	 Cities.cities.stream()
	    	 		.filter((n) -> n.startsWith(start))
	    	 		.forEach((n) -> System.out.print("\nCities Name, which starts with '"+start +"' is : " + n));


	    	 /*List<String> citiesList =  Cities.cities.stream().limit(atmost)
	    			 					 .filter((n) -> n.startsWith(start)).collect(Collectors.toList());*/
	    	 
	    	 List<String> citiesList = Cities.cities.stream()
 					 .filter(startsWithJ).collect(Collectors.toList());
	    	 
	     
	    	
	    	
	    	return citiesList;

	    }
	    
	 
	    

}
