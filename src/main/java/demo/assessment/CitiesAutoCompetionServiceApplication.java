package demo.assessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CitiesAutoCompetionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CitiesAutoCompetionServiceApplication.class, args);
	}
}
